**Criação do route53 controller**

 
```
curl -o route53-policy.json https://rafaelmartinscursoeks.s3.amazonaws.com/P5_route53_controller_r53_policy.txt
```

```
aws iam create-policy \
  --policy-name Route53IAMPolicy \
  --policy-document file://route53-policy.json
```

```
export PolicyR53=$(aws iam list-policies --query 'Policies[?PolicyName==`Route53IAMPolicy`].Arn' --output text)
```

```
eksctl create iamserviceaccount \
 --name external-dns \
 --namespace kube-system \
 --cluster $CLUSTERNAME \
 --attach-policy-arn $PolicyR53 \
 --approve
```

```
curl -O "https://rafaelmartinscursoeks.s3.amazonaws.com/route53_deployment.yaml"
sed "s/eks.yourdomain.cloud/<seu_dominio_pubico.com>/g" route53_deployment.yaml | tee route53_deployment.yaml
kubectl apply -f route53_deployment.yaml
```



***Teste***

```
kubectl logs -n kube-system $(kubectl get po -n kube-system | egrep -o external-dns[a-zA-Z0-9-]+)
```






