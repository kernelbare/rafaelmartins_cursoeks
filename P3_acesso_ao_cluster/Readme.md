**Continuação da configuração do AdminUser**


***Criação de uma accessKey para o usuário admin***

```
aws iam create-access-key --user-name $USER | tee ~/$USER.json
```


***Criação de um iamidentitymapping para o usuário admin***

```
eksctl create iamidentitymapping --cluster $CLUSTERNAME --arn arn:aws:iam::${ACCOUNT_ID}:role/k8sAdmin --username admin --group system:masters
```

***configuração do aws-config e aws-credentials para o usuario admin***

```
cat << EoF >> ~/.aws/config
[profile admin]
role_arn=arn:aws:iam::${ACCOUNT_ID}:role/k8sAdmin
source_profile=eksAdmin
EoF
```

```
### VERIFICAR SE O jq ESTA INSTALADO
cat << EoF >> ~/.aws/credentials
[eksAdmin]
aws_access_key_id=$(jq -r .AccessKey.AccessKeyId ~/$USER.json)
aws_secret_access_key=$(jq -r .AccessKey.SecretAccessKey ~/$USER.json)
EoF
```

***Configuração do kubeconfig***

```
echo > ~/.kube/config
aws eks --region $AWS_REGION update-kubeconfig --name $CLUSTERNAME
echo export KUBECONFIG=~/.kube/config >> ~/.bash_profile
. ~/.bash_profile
```

```
USE_ADMIN_AWS_CRED="      - --profile
      - admin
      env:
      - name: AWS_STS_REGIONAL_ENDPOINTS
        value: regional
"
```

```
REPLACE_LINE=$(grep -Fn 'command: aws' $KUBECONFIG|cut -d':' -f1)
ex -s -c "${REPLACE_LINE}i|$USE_ADMIN_AWS_CRED" -c x $KUBECONFIG
```

***Teste***

```
kubectl get namespaces
```

