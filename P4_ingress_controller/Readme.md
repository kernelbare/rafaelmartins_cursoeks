**Criação do ingress controller**


```
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/${ALB_INGRESS_VERSION}/docs/examples/rbac-role.yaml
```


```
aws ec2 create-tags --resources subnet-id_a_xxxxx subnet-id_c_xxxxx --tags Key="kubernetes.io/role/internal-elb",Value=1
```

***(subnets-xxxx usadas na criação do cluster)***
***Atentar a tag kubernetes.io/role/internal-elb se cluster for privado.***

***kubernetes.io/role/elb para cluster publico***

https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html

```
eksctl utils associate-iam-oidc-provider --cluster=$CLUSTERNAME --approve
```

```
eksctl create iamserviceaccount --cluster=$CLUSTERNAME --namespace=kube-system --name=alb-ingress-controller --attach-policy-arn=$PolicyARN --override-existing-serviceaccounts --approve
```

```
curl -sS "https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/${ALB_INGRESS_VERSION}/docs/examples/alb-ingress-controller.yaml" | sed "s/# - --cluster-name=devCluster/- --cluster-name=$CLUSTERNAME/g" | kubectl apply -f -
```


***Teste***

```
kubectl logs -n kube-system $(kubectl get po -n kube-system | egrep -o alb-ingress[a-zA-Z0-9-]+)
```






