**Criando o cluster**

```
eksctl create cluster -f eks-cluster-<prv/pub>.yaml
```

**_Obs: A criação do cluster + nós gerenciados pode levar até 30 minutos..._**

**Exportando o nome do cluster como variável**

```
echo 'export CLUSTERNAME="nome_do_seu_cluster"' >> ~/.bash_profile
. ~/.bash_profile
```

https://eksctl.io/usage/creating-and-managing-clusters/
