**Requisitos para criação do cluster (Maquina cliente)**

AWSCli
- https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

kubectl
- https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html

eksctl
- https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html

jq
- `sudo yum install jq`
- `sudo apt install jq`

curl
- `sudo yum install curl`
- `sudo apt install curl`

**Mais links uteis**

Subindo uma Ec2 na AWS
- https://docs.aws.amazon.com/pt_br/efs/latest/ug/gs-step-one-create-ec2-resources.ht



**Referencias**

Alb ingress controller documentation
https://kubernetes-sigs.github.io/aws-load-balancer-controller/latest/

EKS workshop
https://www.eksworkshop.com/

Application load balancer EKS
https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html

EKSctl networking
https://eksctl.io/usage/vpc-networking/

Cluster VPC considerations
https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html

EKS workshop IAM users
https://www.eksworkshop.com/beginner/091_iam-groups/create_iam_users/













